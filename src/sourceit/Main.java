package sourceit;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Info[] infos = Generator.generate();
        Scanner in = new Scanner(System.in);
        System.out.println("Введите сумму в гривне:");
        int uah = Integer.parseInt(in.nextLine());
        for (Info info : infos) {
            System.out.println(String.format("Согласно курса обмена в: %s%n выйдет %.2f USD", info.getName(), info.obmen(uah)));
        }
    }
}



