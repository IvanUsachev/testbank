package sourceit;

public class Info {
    private String name;
    private float kurs;

    public Info(String name, float kurs) {
        this.name = name;
        this.kurs = kurs;
    }

    public float obmen(int uah) {
        return uah / kurs;
    }

    public String getName() {
        return name;
    }
 }
