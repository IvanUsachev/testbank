package sourceit;

public class Generator {
    public static Info[] generate() {
        Info[] infos = new Info[7];
        infos[0] = new Bank("Банк № 1", 25.5f, 150000);
        infos[1] = new Bank("Банк № 2", 25.6f, 150000);
        infos[2] = new Bank("Банк № 3", 25.7f, 150000);
        infos[3] = new Obmenka("Обменка № 1", 25.4f, 5000);
        infos[4] = new Obmenka("Обменка № 2", 25.3f, 4000);
        infos[5] = new Obmenka("Обменка № 3", 25.2f, 3000);
        infos[6] = new Blackmarket("Черный рынок",25.1f);
        return infos;
    }
}

